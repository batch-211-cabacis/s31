// NODE JS INTRO

// use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// the "http" module lets Node.js transfer data using the (http = hypertext transfer protocol)
// the http module is a set of indivdual files that contains code to create a "component" that helps establish data transfer between applications
// HTTP is a protocol that allow the fetching of resources such as HTMl documents
// clients (browser) and servers (Node.js/express.js applications) communicates by exchanging individual messages
// the messages sent by the client , usually a web browser are called requests
// the messages sent by the server as an answer are called responses. 

let http = require("http");

// http module has a create server method that accepts a function as an argument and allows for a creation of a server
// the arguments passed in the function are request and response objects that contains methods that allow us to receive requests form the client and send responses back to it

http.createServer(function (request, response) {

	/*
		use the writeHead() method to:
		- set a status code for the response - a 200 means OK
		- set the content-type of the response as a plain text message
	*/
	response.writeHead(200, {'Content-Type': 'text/plain'})

	// send the response with a text contentg "Hello World"
	response.end('Hello World');

}).listen(4000)

// a port is a virtual point where network connections start and end
// each port is associated with a specific process or service
// the server will be assigned to port 4000 via the "listen" method where the server will listen to any requests that are sent to it, eventually communicating with our server

console.log('Server running at local host: 4000');